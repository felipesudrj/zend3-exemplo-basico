<?php
namespace Compras\Repository;

use Doctrine\ORM\EntityRepository;

class ComprasRepository extends EntityRepository
{

	public function buscarTodos()
	{
		$arrDados = $this->findAll();
		$arrRetorno = [];
		foreach($arrDados as $cd_index=>$arrValor){
			$arrRetorno[$cd_index]['id'] = $arrValor->getId();
			$arrRetorno[$cd_index]['ds_nome'] = 'teste';
		}
		return $arrRetorno;
	}
}