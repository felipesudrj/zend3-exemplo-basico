<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Compras\Controller;

use Compras\Entity\Compra;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class IndexController extends AbstractActionController
{
    public function indexAction()
    {
        return new ViewModel();
    }
    public function editarAction()
    {

        $container = $this->getEvent()
            ->getApplication()
            ->getServiceManager();
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $repository = $entityManager->getRepository(Compra::class);
        $arrRegistros = $repository->buscarTodos();

        return new ViewModel(
            array(
                'arrRegistros' => $arrRegistros,
            )
        );

    }

}
