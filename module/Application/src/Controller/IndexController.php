<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Application\Entity\Post;
class IndexController extends AbstractActionController
{
    public function indexAction()
    {
        return new ViewModel();
    }
    public function editarAction()
    {

        $container = $this->getEvent()
            ->getApplication()
            ->getServiceManager();

        $entityManager = $container->get('doctrine.entitymanager.orm_default');


        /*
        $post = new Post();
        $post->setTitle('Top 10+ Books about Zend Framework 3');
        $post->setContent('Post body goes here');
        $post->setStatus(Post::STATUS_PUBLISHED);
        $currentDate = date('Y-m-d H:i:s');
        $post->setDateCreated($currentDate);

        // Add the entity to entity manager.
        $entityManager->persist($post);

        // Apply changes to database.
        $entityManager->flush();
        */
        $repository = $entityManager->getRepository(Post::class);

        $arrRegistros = $repository->findAll();

        dump($arrRegistros);

        return new ViewModel();

    }

}
